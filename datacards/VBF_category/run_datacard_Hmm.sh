#!/bin/bash

dcard_h_peak="hist__dimuon_invmass_h_peak_cat5__dnnPisa_pred"
dcard_h_sideband="hist__dimuon_invmass_h_sideband_cat5__dnnPisa_pred"
wd=`pwd`
year=2016
dataset=data_obs
combineCards.py $dcard_h_peak.txt $dcard_h_sideband.txt > comb_$year.txt

#step 2 create combined datacards with three regions
text2workspace.py -D $dataset comb_$year.txt --channel-masks -o workspace_comb_withmasks_$year.root

#step 3: save workspace, fit to data in the CR regions with r fixed to 1, signal region masked, and produce output higgsCombineTest.MultiDimFit.mH120.root
echo "s3"
echo "s3.1"
rm higgsCombineTest.MultiDimFit.mH120.root
combine -D $dataset -M MultiDimFit --saveWorkspace --setParameters mask_ch1=1,r=1 --freezeParameters r workspace_comb_withmasks_$year.root --verbose 9

#rename of the output
echo "s3.2"
mv higgsCombineTest.MultiDimFit.mH120.root higgsCombineTest.MultiDimFit.SRmasked.root

#step 5: significance produced from workspace in higgsCombineTest.MultiDimFit.mH120.root, loading the snapshot MultiDimFit before fitting
combine -M Significance --signif -m 125 higgsCombineTest.MultiDimFit.SRmasked.root --snapshotName MultiDimFit -t -1 --expectSignal=1 --saveWorkspace --saveToys --bypassFrequentistFit --setParameters mask_ch1=0 --floatParameters r --toysFrequentist --verbose 9
cd $wd
